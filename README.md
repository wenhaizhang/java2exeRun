# java2exeRun

#### 项目介绍
将java最终打包成exe的空项目

#### 使用方法
1. 下载本项目，并在src下实现自己的功能，最终使用`mvn -Dmaven.test.skip=true clean package`命令打包成exe。Main方法在App.java中时，所有配置都可不做修改。
2. 自己项目中实现所有逻辑，将本项目的pom.xml中的plugin信息复制到自己项目中，并修改必要的配置信息。